package com.yueya.im.manager.handler;

import akka.actor.ActorRef;
import akka.actor.Props;
import com.yueya.im.common.api.CmdHandler;
import com.yueya.im.common.api.ImContext;
import com.yueya.im.manager.actors.CollectActor;
import com.yueya.im.manager.messages.MsgType;
import com.yueya.im.server.actors.ImActorSystem;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.UUID;

import static com.yueya.im.common.constant.ImInfo.COLLECT_ACTOR_NAME;

@Component
public class ConsoleMsgHandler implements CmdHandler {
    private ActorRef actorRef;
    @Override
    public boolean preMessage(int msgType, byte[] message, long msgId) {
        return true;
    }

    @Override
    public Serializable onMessage(ImContext ctx, int msgType, byte[] message, long msgId) {
       if(msgType == 1000){
           if (actorRef == null) {
               actorRef = ImActorSystem.getInstance().getActorSystem()
                       .actorOf(Props.create(CollectActor.class),COLLECT_ACTOR_NAME);
           }
           String device = UUID.randomUUID().toString();
           ctx.connect("console",device);
           ctx.sendToClient(MsgType.CONNECT,"{}");
       }else if(msgType == 1003) {
           actorRef.tell("console",actorRef);
       }
       return null;
    }

    @Override
    public void postMessage(ImContext ctx, int msgType, byte[] message, long msgId, Serializable obj) {

    }


}

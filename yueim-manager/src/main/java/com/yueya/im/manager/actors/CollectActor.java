package com.yueya.im.manager.actors;

import akka.actor.AbstractActor;
import akka.japi.pf.ReceiveBuilder;
import com.yueya.im.common.constant.ImInfo;
import com.yueya.im.common.model.ActorMsg;
import com.yueya.im.common.model.ActorMsg.ActorCmd;
import com.yueya.im.server.actors.ImActorSystem;

public class CollectActor extends AbstractActor {
    @Override
    public Receive createReceive() {
        return ReceiveBuilder
                .create()
                .match(ActorCmd.class, cmd -> {
                    if (cmd.getCmdType() == ActorMsg.CmdType.CONSOLE) {
                        //手段节点反馈，通知终端
                        ActorCmd consoleCmd = ActorCmd.newBuilder()
                                .setCmdType(ActorMsg.CmdType.RECEIVE)
                                .setMessage(cmd.getMessage())
                                .setMsgType(1002)
                                .build();
                        ImActorSystem.getInstance()
                                .getUsers()
                                .forEach(r -> r.forEach((k, v) -> v.getActor().tell(consoleCmd, getSelf())));
                    }
                }).match(String.class, msg -> {
                    //发送收集信息命令
                    ActorCmd cmd = ActorCmd.newBuilder()
                            .setCmdType(ActorMsg.CmdType.CONSOLE)
                            .build();
                    ImActorSystem.getInstance().getAllNodes()
                            .forEach(r -> {
                                ImActorSystem.getInstance().getActorSystem()
                                        .actorSelection(r + "/user/" + ImInfo.CONSOLE_ACTOR_NAME)
                                        .tell(cmd, getSelf());
                            });
                }).build();
    }
}

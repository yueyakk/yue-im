package com.yueya.im.manager.handler;

import com.yueya.im.common.api.BusinessInfoProvider;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ConsoleInfoProvider implements BusinessInfoProvider {
    @Override
    public List<String> member(String groupId) {
        return null;
    }

    @Override
    public List<String> friends(String userId) {
        return null;
    }
}

var chat = (function(){
    var table;
    var list = [];
    var socket = window.websocketClient;
    var init = function(options) {
        table = options.table;
        socket.on('message', function (msg) {
            //文本信息
            if (msg.msgType == 1002) {
                updateMessage(msg, msg.msgType);
            }
        });
        socket.on('connect', function (msg) {
            socket.send('1000', {
                'device': uuid()
            }, function (resp) {
                layer.alert("连接成功");
            }, 1001)
        });
        socket.connect({
            server: im_server_addr,
        })
    }
    var time = function time(time) {
        var date = new Date(time + 8 * 3600 * 1000); // 增加8小时
        return date.toJSON().substr(0, 16).replace('T', ' ');
    }

    var updateMessage = function(msg,msgType){
        var has = false;
        console.log(msg);
        msg.jvmFreeMemory = (msg.jvmFreeMemory/1024/1024).toFixed(2)+'MB';
        msg.jvmMaxMemory = (msg.jvmMaxMemory/1024/1024).toFixed(2)+'MB';
        msg.cacheRoutes = msg.cacheRoutes+"/"+msg.maxRoutes;
        for (var i = 0; i < list.length; i++) {
            if(list[i].nodeAddress == msg.nodeAddress){
                has = true;
                list[i] = msg;
            }
        }
        if(!has){
            list.push(msg);
        }
        table.reload({data:list});
    }
    var sendMsg = function() {
        socket.send(1003,'');
    }
    var uuid = function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });

    }
    return{
        init:init,
        sendMsg:sendMsg
    }
})();
package com.yueya.im.server.handler;

import com.yueya.im.common.api.CmdHandler;
import com.yueya.im.common.api.ImContext;

import java.io.Serializable;

import static com.yueya.im.common.constant.YueMsgCode.MSG_HEART_BEAT_PONG;

public class DefaultBeatHandler implements CmdHandler {
    @Override
    public boolean preMessage(int msgType, byte[] message, long msgId) {
        return true;
    }

    @Override
    public Serializable onMessage(ImContext ctx, int msgType, byte[] message, long msgId) {
        ctx.sendToClient(MSG_HEART_BEAT_PONG,"");
        return null;
    }

    @Override
    public void postMessage(ImContext ctx, int msgType, byte[] message, long msgId, Serializable obj) {

    }

}

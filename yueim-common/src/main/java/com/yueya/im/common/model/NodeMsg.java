package com.yueya.im.common.model;

public class NodeMsg {
    private String nodeAddress;
    private long jvmFreeMemory;
    private long jvmMaxMemory;
    private int devices;
    private int users;
    private int cacheRoutes;
    private long sendMsgs;
    private int maxRoutes;

    public long getJvmFreeMemory() {
        return jvmFreeMemory;
    }

    public void setJvmFreeMemory(long jvmFreeMemory) {
        this.jvmFreeMemory = jvmFreeMemory;
    }

    public long getJvmMaxMemory() {
        return jvmMaxMemory;
    }

    public void setJvmMaxMemory(long jvmMaxMemory) {
        this.jvmMaxMemory = jvmMaxMemory;
    }


    public int getCacheRoutes() {
        return cacheRoutes;
    }

    public void setCacheRoutes(int cacheRoutes) {
        this.cacheRoutes = cacheRoutes;
    }

    public long getSendMsgs() {
        return sendMsgs;
    }

    public void setSendMsgs(long sendMsgs) {
        this.sendMsgs = sendMsgs;
    }

    public int getUsers() {
        return users;
    }

    public void setUsers(int users) {
        this.users = users;
    }

    public int getDevices() {
        return devices;
    }

    public void setDevices(int devices) {
        this.devices = devices;
    }

    public String getNodeAddress() {
        return nodeAddress;
    }

    public void setNodeAddress(String nodeAddress) {
        this.nodeAddress = nodeAddress;
    }

    public int getMaxRoutes() {
        return maxRoutes;
    }

    public void setMaxRoutes(int maxRoutes) {
        this.maxRoutes = maxRoutes;
    }
}

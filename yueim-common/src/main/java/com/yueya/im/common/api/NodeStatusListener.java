package com.yueya.im.common.api;

public interface NodeStatusListener {
    /**
     * 节点上线触发
     */
    void onMerUp(String nodeIp);

    /**
     * 节点下线触发
     */
    void onMerDown(String nodeIp);
}

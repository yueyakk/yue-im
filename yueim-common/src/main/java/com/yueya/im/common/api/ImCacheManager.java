package com.yueya.im.common.api;

public interface ImCacheManager {
    ImCache getCache(String cacheName);
}

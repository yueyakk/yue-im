package com.yueya.im.common.api;

/**
 * 接收rest接口消息时的回调
 */
public interface RestMsgListener {
    boolean preMessage(String text);
    void postMessage(String text);
}

package com.yueya.im.common.api;


import java.util.List;

public interface BusinessInfoProvider {

    List<String> member(String groupId);

    List<String> friends(String userId);

}

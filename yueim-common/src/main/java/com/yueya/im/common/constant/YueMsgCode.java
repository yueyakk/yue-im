package com.yueya.im.common.constant;

public class YueMsgCode {
    public static final int MSG_HEART_BEAT_PING = 0;
    public static final int MSG_HEART_BEAT_PONG = 1;
    public static final int REST_MSG_POINT = 2;
    public static final int REST_MSG_GROUP = 3;
}

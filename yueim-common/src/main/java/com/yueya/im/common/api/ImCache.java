package com.yueya.im.common.api;

import java.util.Collection;

public interface ImCache<T> {
    T get(String key);
    String set(String key, T value);
    boolean containsKey(String key);
    void put(String id, T value);
    void remove(String id);
    int size();
    Collection<T> values();
}

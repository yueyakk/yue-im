package com.yueya.im.node.handler.impl;

import com.yueya.im.common.api.CmdHandler;
import com.yueya.im.common.api.ImContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

public class DefaultHandler implements CmdHandler {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public boolean preMessage(int msgType, byte[] message, long msgId) {
        return true;
    }

    @Override
    public Serializable onMessage(ImContext ctx, int msgType, byte[] message, long msgId) {
        return null;
    }

    @Override
    public void postMessage(ImContext ctx, int msgType, byte[] message, long msgId, Serializable obj) {

    }

}
